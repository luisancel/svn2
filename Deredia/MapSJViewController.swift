//
//  MapSJViewController.swift
//  Deredia
//
//  Created by Luis Eduardo Sanchez Celedon on 12/17/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import Foundation

import GoogleMaps
import GooglePlaces


class MapSJViewController: UIViewController, GMSMapViewDelegate{
 
    var idEsculturaSeleccionada : String = ""
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        //        // API KEY de Google generado con cuenta ICE
        //GMSServices.provideAPIKey("AIzaSyCX88gh2ii1o2WwAGAJC1BIa0v59Ggn-8Q")
        //GMSPlacesClient.provideAPIKey("AIzaSyCX88gh2ii1o2WwAGAJC1BIa0v59Ggn-8Q")
        cargarMapa()
    }

    
    func cargarMapa(){
        let camara = GMSCameraPosition.camera(withLatitude: 9.935379,
                                              longitude: -84.079790, zoom: 18)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camara)
        
        mapView.delegate = self
        
        mapView.setMinZoom(18, maxZoom: 25)
        
        //Limites del mapa
        let coordenada1 = CLLocationCoordinate2D(latitude: 9.936237, longitude: -84.080531)
        let coordenada2 = CLLocationCoordinate2D(latitude: 9.931987, longitude: -84.071101)
        
        let bounds = GMSCoordinateBounds(coordinate: coordenada1, coordinate: coordenada2)
        
        mapView.cameraTargetBounds = bounds
        
        view = mapView
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        cargarPunto(latitud: 9.935414, lontitud: -84.079688, titulo: "1", mapView: mapView, icon: "1-2")
        cargarPunto(latitud: 9.934672, lontitud: -84.079886, titulo: "2", mapView: mapView, icon: "2-2")
        cargarPunto(latitud: 9.934218, lontitud: -84.080468, titulo: "3", mapView: mapView, icon: "3-2")
        cargarPunto(latitud: 9.934138, lontitud: -84.079805, titulo: "4", mapView: mapView, icon: "4-2")
        cargarPunto(latitud: 9.933964, lontitud: -84.078830, titulo: "5", mapView: mapView, icon: "5-2")
        cargarPunto(latitud: 9.932979, lontitud: -84.079002, titulo: "6", mapView: mapView, icon: "6-2")
        cargarPunto(latitud: 9.933938, lontitud: -84.078517, titulo: "7", mapView: mapView, icon: "7-2")
        cargarPunto(latitud: 9.933886, lontitud: -84.077821, titulo: "8", mapView: mapView, icon: "8-2")
        cargarPunto(latitud: 9.933315, lontitud: -84.077697, titulo: "9", mapView: mapView, icon: "9-2")
        cargarPunto(latitud: 9.933188, lontitud: -84.077224, titulo: "10", mapView: mapView, icon: "10-2")
        cargarPunto(latitud: 9.933551, lontitud: -84.077365, titulo: "11", mapView: mapView, icon: "11-2")
        cargarPunto(latitud: 9.933381, lontitud: -84.076988, titulo: "12", mapView: mapView, icon: "12-2")
        cargarPunto(latitud: 9.933590, lontitud: -84.076827, titulo: "13", mapView: mapView, icon: "13-2")
        cargarPunto(latitud: 9.933790, lontitud: -84.077237, titulo: "14", mapView: mapView, icon: "14-2")
        cargarPunto(latitud: 9.933751, lontitud: -84.076773, titulo: "15", mapView: mapView, icon: "15-2")
        cargarPunto(latitud: 9.933748, lontitud: -84.076301, titulo: "16", mapView: mapView, icon: "16-2")
        cargarPunto(latitud: 9.933706, lontitud: -84.075878, titulo: "17", mapView: mapView, icon: "17-2")
        cargarPunto(latitud: 9.933671, lontitud: -84.075466, titulo: "18", mapView: mapView, icon: "18-2")
        cargarPunto(latitud: 9.933627, lontitud: -84.074980, titulo: "19", mapView: mapView, icon: "19-2")
        cargarPunto(latitud: 9.933362, lontitud: -84.072688, titulo: "20", mapView: mapView, icon: "20-2")
        cargarPunto(latitud: 9.933310, lontitud: -84.072562, titulo: "21", mapView: mapView, icon: "21-2")
        cargarPunto(latitud: 9.933312, lontitud: -84.072422, titulo: "22", mapView: mapView, icon: "22-2")
        cargarPunto(latitud: 9.933051, lontitud: -84.072539, titulo: "23", mapView: mapView, icon: "23-2")
        cargarPunto(latitud: 9.932843, lontitud: -84.072378, titulo: "24", mapView: mapView, icon: "24-2")
        cargarPunto(latitud: 9.932631, lontitud: -84.072560, titulo: "25", mapView: mapView, icon: "25-2")
        cargarPunto(latitud: 9.932542, lontitud: -84.072171, titulo: "26", mapView: mapView, icon: "26-2")
        cargarPunto(latitud: 9.932874, lontitud: -84.072043, titulo: "27", mapView: mapView, icon: "27-2")

    }
    
    func cargarPunto(latitud: Double, lontitud: Double, titulo: String, mapView: GMSMapView, icon: String){
        let currentLocation = CLLocationCoordinate2DMake(latitud,lontitud)
        let marker = GMSMarker(position: currentLocation)
        marker.title = titulo
        marker.map = mapView
        marker.icon = UIImage(named: icon)
    }

    // Set the status bar style to complement night-mode.
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //Accion de tap al marcador
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.idEsculturaSeleccionada = marker.title!
        
        performSegue(withIdentifier: "id_mapa_audio", sender: self)
        
        return true;
    }
    
    //Se llama antes de performSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let infoObraVC = segue.destination as! SanJoseInfoEsculturaViewController //Casteo
        
        //Seteo del parametro a enviar
        infoObraVC.idEscultura = self.idEsculturaSeleccionada
    }
    
    // Para esconder el statusbar
    override var prefersStatusBarHidden: Bool{
        return true
    }
}

