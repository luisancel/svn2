//
//  MuseoOroTableCell.swift
//  Deredia
//
//  Created by ice on 22/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class MuseoOroTableCell: UITableViewCell {

    
    @IBOutlet weak var imgObraOro: UIImageView!
    
    @IBOutlet weak var lblNumeroObraOro: UILabel!
    
    @IBOutlet weak var lblNombreObra: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
