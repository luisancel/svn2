//
//  MuseoOroAPI.swift
//  Deredia
//
//  Created by ice on 18/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import Foundation

class MuseoOroAPI{
    let baseURL = "http://servrecursosapl.ice.go.cr/wsderedia/ws/"
    var esculturas = [Escultura]()
    
    init() {
        let operacion = "media/listaroro"
        
        guard let url = URL(string: (baseURL + operacion)) else { return }
        
        URLSession.shared.dataTask(with: url){
            (data, response, error) in
            
            //Chequeo del error y que el status sea el 200
            if error != nil {
                print(error!.localizedDescription)
            }
            
            guard let data = data else { return }
            
            //Decodificacion y parseo del JSON
            do {
                //Decodificacion JSON a arreglo de esculturas
                self.esculturas = try JSONDecoder().decode([Escultura].self, from: data)
                
                //Get back to the main queue
                DispatchQueue.main.async {
                    //print(articlesData)
                    //self.esculturas = esculturas
                    print(self.esculturas[3].nombre)
                    //self.collectionView?.reloadData()
                }
                
            } catch let jsonError {
                print(jsonError)
            }
            }.resume()
        
    }
}
