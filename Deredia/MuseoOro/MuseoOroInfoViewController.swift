//
//  MuseoOroInfoViewController.swift
//  Deredia
//
//  Created by Luis Eduardo Sanchez Celedon on 12/28/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class MuseoOroInfoViewController: UIViewController {

    @IBOutlet weak var tvOroActividades: UITextView!
    
    @IBOutlet weak var tvOroEspacioMultifucnal: UITextView!
    
    @IBAction func accionComoLlegar(_ sender: Any) {
        let urlWazeApp = URL(string: "waze://")
        
        if (UIApplication.shared.canOpenURL(urlWazeApp!)){
            print ("Se puede abrir waze")
            // Waze esta instalado, se abre la navegación
            let urlStr = String(format: "waze://?ll=%f,%f&navigate=yes", 9.933575, -84.076603)
            UIApplication.shared.open(URL(string: urlStr)!)
        }else{
            let alert = UIAlertController(title: "Error", message: "No tienes Waze instalado",         preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                //Cancel Action
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (Locale.current.languageCode == "en"){
        tvOroActividades.text = "The Museums of the Central Bank are the museological space that allows delving into the topics developed in the great exhibition by Jiménez Deredia in San José:  the strength and universality of the sphere, which unfolds outdoors, in the streets and squares of the historic center of the city of San Jose.\n\nIn the multifunctional space of the building\'s facilities, the philosophy, the art, and thoughts of the artist are explained. During the most recent years, the Costa Rican artist has tried to decode the cultural identity of his people. With the poetic unveiling of the Boruca sphere, Deredian sculptures show that our culture has unprecedented wisdom to offer a contribution and an alternative to the complexity of a globalized world.\n\nThe Gold Museum, located inside the facilities of the Central Bank Museums, is essentially comprised by works created by the ancient Boruca culture, a civilization that elaborated the stone spheres. This is a unique opportunity to reflect on the close spiritual and formal relationship between Deredian art and the first Costa Ricans who inhabited our territory for thousands of years."
            
            tvOroEspacioMultifucnal.text = "The exhibition consists of 32 pictures of the sculptures of Jiménez Deredia made by the great Italian photographer Tommaso Malfanti. There are explanatory texts of the concepts of time, space, transmutative symbolism, the sphere and the journey of life, and a series of bronze sculptures made by the artist.\n\nAs the sculptor looks for new options for life in the modern world,  Jiménez Deredia\'s project is deeply ethical. His work delves into the buried symbols of the past to find a light that recovers the entirety of our identity, and in this way, projects an alternative of life for modern man."
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
