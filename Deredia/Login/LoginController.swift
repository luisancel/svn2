//
//  LoginController.swift
//  Deredia
//
//  Created by ice on 24/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class LoginController: UIViewController, UITextFieldDelegate{

//    let baseURL = "http://201.203.21.234/wsderedia/ws/"
    let baseURL = "http://servrecursosapl.ice.go.cr/wsderedia/ws/"

    @IBOutlet weak var vwPanel: UIView!
    
    @IBOutlet weak var btnContinuar: UIButton!
    
    @IBOutlet weak var txtNombre: UITextField!
    
    @IBOutlet weak var txtTelefono: UITextField!
    
    var activeTextField : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        vwPanel.layer.cornerRadius = 10.0
        
        btnContinuar.layer.cornerRadius = 10.0
        
        txtNombre.delegate = self
        txtTelefono.delegate = self
        
        let center: NotificationCenter = NotificationCenter.default;
        center.addObserver(self, selector: #selector(self.keyboardDidShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        center.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)

        
    }
    
    @IBAction func terminosAction(_ sender: Any) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc : TerminosViewController = storyboard.instantiateViewController(withIdentifier: "TerminosViewController") as! TerminosViewController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func keyboardDidShow(notification: Notification) {
        let info: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardY = self.view.frame.size.height - keyboardSize.height
        let editingTextFieldY:CGFloat! = self.activeTextField?.frame.origin.y
        
        if self.view.frame.origin.y >= 0{
            if editingTextFieldY != nil {
                if editingTextFieldY < keyboardY + 60{
                    UIView.animate(withDuration: 0.20, delay: 0.0, options:
                        UIView.AnimationOptions.curveEaseIn, animations: {
                    
                            self.view.frame = CGRect(x: 0, y: self.btnContinuar.frame.origin.y - (325), width: self.view.bounds.width, height: self.view.bounds.height)
                    }, completion: nil)
                }
            }
        }
        
    }

    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.20, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y:0, width: self.view.bounds.width, height: self.view.bounds.height)
        }, completion: nil)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtNombre.resignFirstResponder()
        txtTelefono.resignFirstResponder()
        
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField

    }
        
    
    @IBAction func btnContinuarAccion(_ sender: Any) {
        let nombre : String = txtNombre.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let telefono: String = txtTelefono.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        if !(nombre.isEmpty) && !(telefono.isEmpty){
            let req : RegistroRequest = RegistroRequest(nombre: nombre.toBase64(), telefono: telefono.toBase64())
            
            registrar(requestData: req){ (error) in
                if let error = error {
                    var mensaje = "Hubo un error en el registro."
                    
                    if (Locale.current.languageCode == "en"){
                        mensaje = "There was an error in your sign up.\nCheck your Internet connection"
                    }
                    
                    let alerta =  UIAlertController(title: "Error", message: mensaje,         preferredStyle: UIAlertController.Style.alert)

                    alerta.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                        //Segue para ir al video
                        self.performSegue(withIdentifier: "continuarVideo", sender: self)
                    }))
                    
                    self.present(alerta, animated: true, completion: nil)
                                        //fatalError(error.localizedDescription)
                }
            }
        }
    }
    
    func registrar(requestData : RegistroRequest, completion:((Error?) -> Void)?){
        
        let url = URL(string: baseURL + "usuario/agregar")
        
        //Especifica el request
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        // Indica codificacion JSON en el envio
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        request.allHTTPHeaderFields = headers
        
        // Codifica el objeto requestData a JSON
        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(requestData)
            // Coloca el json en el body
            request.httpBody = jsonData
            
            //print("jsonData: ", String(data: request.httpBody!, encoding: .utf8) ?? "no body data")
        } catch {
            completion?(error)
        }
        
        // Crea una sesion HTTP para enviar la solicitud POST con el JSON interno
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            guard responseError == nil else {
                completion?(responseError!)
                return
            }
            
            // Valida su existe informacion legible en los datos de respuesta
            //el _ indica que se usara solo para validacion, su valor no se usará
            if let data = responseData, let _ = String(data: data, encoding: .utf8) {
                //print("response: ", utf8Representation)
                do{
                    let registro = try JSONDecoder().decode(RegistroResponse.self, from: data)
                    
                    if (registro.id > 0){
                        UserDefaults.standard.set(String(registro.id), forKey: "idBD")
                        UserDefaults.standard.set(String(registro.token), forKey: "token")
                        
                        let id = String(registro.id)
                        print(id + " " + registro.token)
                    }
                }catch{
                    print("Error de parseo \(error).")
                }
            } else {
                print("Informacion no legible en respuesta")
            }
        }
        task.resume()
    }
    
    // Para esconder el statusbar
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    // Remueve el observer
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}

extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}
