//
//  PreviewVideoViewController.swift
//  Deredia
//
//  Created by Luis Eduardo Sanchez Celedon on 12/17/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//


import UIKit
import AVFoundation



class PreviewVideoViewController: UIViewController {
    @IBOutlet weak var viewVideo: UIView!
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    
    @IBOutlet weak var btnSaltar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "http://servrecursosapl.ice.go.cr/pubres/deredia/video/video.mp4")!
        
        player = AVPlayer(url: url)
        
        //Notificador para cuando el video ha terminado
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: nil) // Agrega el observador

        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resize
        
        //Oculta el boton por 2 segundos y lo muestra
        self.btnSaltar.isHidden = true
        self.btnSaltar.layer.cornerRadius = 10.0
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.btnSaltar.isHidden = false
        }
        
        viewVideo.layer.addSublayer(playerLayer)
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        
        /*if (!CheckInternet.Connection()){
            var mensaje = "Revisa tu conexion a Internet"
            
            if (Locale.current.languageCode == "en"){
                mensaje = "Check your Internet Connection"
            }
            
            let alerta =  UIAlertController(title: "Error", message: mensaje, preferredStyle: UIAlertController.Style.alert)
            
            alerta.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                self.performSegue(withIdentifier: "videoAPrincipal", sender: self)
            }))
            
            self.present(alerta, animated: true, completion: nil)
        }*/
        player.play()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        player.pause()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        playerLayer.frame = viewVideo.bounds
    }
    
    // Manejo de notificacion
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        performSegue(withIdentifier: "videoAPrincipal", sender: self)
    }
    // Remueve el observer
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // Para esconder el statusbar
    override var prefersStatusBarHidden: Bool{
        return true
    }

}

