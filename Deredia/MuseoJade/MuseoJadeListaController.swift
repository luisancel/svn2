//
//  MuseoJadeListaController.swift
//  Deredia
//
//  Created by ice on 20/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//
import UIKit
import Kingfisher

class MuseoJadeListaController: UIViewController {
    
    @IBOutlet weak var tblListaEsculturas: UITableView!
    
    var isErrorConsulta = false
    var isErrorComunicacion = false
    
    var esculturas: [Escultura] = []
    //let baseURL = "http://201.203.21.234/wsderedia/ws/"
    let baseURL = "http://servrecursosapl.ice.go.cr/wsderedia/ws/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //esculturas = MuseoJadeAPI().esculturas
        
        tblListaEsculturas.delegate = self
        tblListaEsculturas.dataSource = self
        
        getEsculturasJade()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (isErrorConsulta){
            var mensaje = "Revisa tu conexión a Internet"
            
            if (Locale.current.languageCode == "en"){
                mensaje = "Check your Internet Connection"
            }
            
            let alerta =  UIAlertController(title: "Error", message: mensaje, preferredStyle: UIAlertController.Style.alert)
            
            alerta.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                //No se hace nada
            }))
            
            self.present(alerta, animated: true, completion: nil)
        }else{
            if (isErrorComunicacion){
                var mensaje = "Error al obtener respuesta del servidor"
                
                if (Locale.current.languageCode == "en"){
                    mensaje = "Error retrieving information from server"
                }
                
                let alerta =  UIAlertController(title: "Error", message: mensaje, preferredStyle: UIAlertController.Style.alert)
                
                alerta.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                    //No hay que hacer nada
                }))
                
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    
    func getEsculturasJade(){
        let operacion = "media/listarjade"
        
        guard let url = URL(string: (baseURL + operacion)) else { return }
        
        URLSession.shared.dataTask(with: url){
            (data, response, error) in
            
            //Chequeo del error y que el status sea el 200
            if error != nil {
                let str = error!.localizedDescription
                if (str.contains("connect to the server")){
                    self.isErrorComunicacion = true
                }else{
                    self.isErrorConsulta = true
                }
                
                return
            }
            
            guard let data = data else { return }
            
            //Decodificacion y parseo del JSON
            do {
                //Decodificacion JSON a arreglo de esculturas
                self.esculturas = try JSONDecoder().decode([Escultura].self, from: data)
                
                //Get back to the main queue
                DispatchQueue.main.async {
                    //print(articlesData)
                    //self.esculturas = esculturas
                    self.tblListaEsculturas.reloadData()
                }
                
            } catch let jsonError {
                print(jsonError)
            }
            }.resume()
    }
    
}

extension MuseoJadeListaController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return esculturas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let escultura = esculturas[indexPath.row]
        
        let celda = tableView.dequeueReusableCell(withIdentifier: "MuseoJadeTableCell") as! MuseoJadeTableCell
        
        //Deshabilitacion de touch
        celda.selectionStyle = .none;
        //celda.isUserInteractionEnabled = false
        
        celda.lblNombreObra.text = "- " + escultura.nombre
        celda.lblNumeroObra.text = String(escultura.id)
        
        let url = URL(string:
            escultura.urlimg1.trimmingCharacters(in: .whitespacesAndNewlines))
        
        celda.imgObra.kf.indicatorType = .activity
        celda.imgObra.kf.setImage(with: url)
        
        return celda
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100.0;//Ancho de la fila
    }
    
    //Seleccion
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var mensaje : String = "Visitá el museo y conocé más de esta obra"
        
        if (Locale.current.languageCode == "en"){
            mensaje = "Visit the museum and learn more about this artwork"
        }
        
        let alerta =  UIAlertController(title: "", message: mensaje, preferredStyle: UIAlertController.Style.alert)
        
        alerta.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            //No se hace nada
        }))
        
        self.present(alerta, animated: true, completion: nil)
    }
    
    // Para esconder el statusbar
    override var prefersStatusBarHidden: Bool{
        return true
    }
}
