//
//  MuseoJadeInfoViewController.swift
//  Deredia
//
//  Created by Luis Eduardo Sanchez Celedon on 12/28/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class MuseoJadeInfoViewController: UIViewController {

    @IBOutlet weak var tvActividades: UITextView!
    
    @IBOutlet weak var tvSobreArtista: UITextView!
    
    @IBOutlet weak var btnComoLlegar: UIButton!
    
    @IBOutlet weak var tvRecorrido: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if (Locale.current.languageCode == "en"){
            tvActividades.text = "The Jade Museum is part of the Jiménez Deredia exhibition in San José.  In this museum, a series of photographs related to the work of the artist will be presented on the eastern side. The pictures were taken by the internationally renowned photographer, Ignacio Guevara. Inside the Museum's facilities, there will be sculptures in marble and bronze made by the Costa Rican sculptor. The public will also have the opportunity to \"live\" the creative process of making a sculpture, participating in interactive workshops for the visually impaired, for children, and for adults."
            
            tvSobreArtista.text = "For the Costa Rican sculptor, exhibiting his artwork in the Jade Museum means to create a link and a reflection on the deep history of Costa Rica, its development, and the formation of its cultural identity.\n\nUnderstanding one\'s cultural identity is the first step in order to deeply understand the heritage that each human being receives from the nation where he/she was born. When the artist speaks of cultural identity, he refers to that common feeling that characterizes human groups, assimilated over the centuries, which determines a specific perception of the world. The deep history is the main way to become aware of, and to identify, the ancestors that inhabit within us.\n\nLet the viewer perceive with his/her own sensitivity the relationship of time, space, and common transmutation between the work of Jiménez Deredia and the pre-Columbian Costa Rican cultures present in the museum."
            
            tvRecorrido.text = "At the entrance, visitors will find the sculpture called Primal Energy, made with white Greek marble. On the right side, a block of jade can be admired. For centuries, our ancestors worked with this material.\n\nThe exhibition continues in the 3 upper levels of the building.  At the entrance and at the exit, in the exhibition halls, there are 9 sculptures cast in bronze that will guide the visitor into a path of reflection and poetry."
        }
    }
    
    @IBAction func btnComoLlegar(_ sender: Any) {
        let urlWazeApp = URL(string: "waze://")
        
        if (UIApplication.shared.canOpenURL(urlWazeApp!)){
            //Waze instalado, se lanza la navegación
            let urlStr = String(format: "waze://?ll=%f,%f&navigate=yes", 9.933358, -84.072835)
            UIApplication.shared.open(URL(string: urlStr)!)
        }else{
            let alert = UIAlertController(title: "Error", message: "No tienes Waze instalado",         preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                //Cancel Action
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
