//
//  InfoObraRequest.swift
//  Deredia
//
//  Created by ice on 27/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class InfoObraRequest: Codable {
    let id : String
    let locale : String
    
    init() {
        self.id = ""
        self.locale = "es"
    }
    
    init(id : String, locale : String) {
        self.id = id
        self.locale = locale
    }
}
