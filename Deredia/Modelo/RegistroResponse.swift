//
//  RegistroResponse.swift
//  Deredia
//
//  Created by ice on 24/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class RegistroResponse: Codable {
    let id: Int
    let mensaje : String
    let nombre : String
    let telefono : String
    let token : String
    
    init(){
        self.id = 0
        self.mensaje = ""
        self.nombre = ""
        self.telefono = ""
        self.token = ""
    }
}
