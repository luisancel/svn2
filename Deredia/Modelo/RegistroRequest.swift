//
//  RegistroRequest.swift
//  Deredia
//
//  Created by ice on 24/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class RegistroRequest: Codable {
    let nombre : String
    let telefono : String
    
    init(){
        self.nombre = ""
        self.telefono = ""
    }
    
    init(nombre : String, telefono : String){
        self.nombre = nombre
        self.telefono = telefono
    }
}
