//
//  Escultura.swift
//  Deredia
//
//  Created by ice on 18/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class Escultura: Codable {
    let desc : String?
    let dimension : String?
    let id : Int
    let locale : String
    let nombre : String
    let peso : String?
    let texto : String?
    let urlimg1 : String
    let urlimg2 : String?
    let urlaudio : String?
    
    init(){
        self.desc = ""
        self.dimension = ""
        self.id = 0
        self.locale = ""
        self.nombre = ""
        self.peso = ""
        self.texto = ""
        self.urlimg1 = ""
        self.urlimg2 = ""
        self.urlaudio = ""
    }
}
