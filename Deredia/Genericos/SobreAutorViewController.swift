//
//  SobreAutorViewController.swift
//  Deredia
//
//  Created by ice on 28/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class SobreAutorViewController: UIViewController {

    @IBOutlet weak var tvSobreAutor: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if (Locale.current.languageCode == "en"){
            tvSobreAutor.text = "Deredia was born on 4th October 1954 in Costa Rica\n\nIn 1976, he opened his art studio in Carrara, where he is permanently based.\n\nDeredia graduated from the Academy of Fine Arts of Carrara, and he attended the School of Architecture at Florence University in Italy.\n\nThe stone spheres created by the Boruca pre-Columbian civilization of Costa Rica are a constant inspiration of his art works.\n\nIn 1985, Deredia created the first Genesis and lay the foundations for his Transmutational Symbolism.\n\nIn 2000, Pope John Paul II commissioned the sculptor to produce a statue of San Marcellino Champagnat, which was placed in San Peter Basilica in the Vatican inside a niche created by Michel Angelo Buonarroti between 1544 and 1564.\n\nDeredia was appointed Correspondent Academician of Sculpture Classes by the Florentine Academy of Art and Design. Throughout this Academy’s history, nominees include Michelangelo, Palladio, Galileo Galilei, and others\n\nIn 2009, the Roman Forum opened its doors to contemporary creations for the first time in history, hosting monumental sculptures of Jiménez Deredia on the Via Sacra (Sacred Road)\n\nThe exhibition in the city of San José continues with a long trajectory started in 2003. Deredia has shown his works in outdoor exhibitions in many cities worldwide, from Florence to Rome, from Valencia to Trapani, La Baule in France, Mexico City, Lucca in Italy and Miami.\n\nDuring his career, this unique Latin American artist and thinker has carved and cast monumental works in marble and bronze for museums and public spaces in Europe, the United States of America, Asia and Latin America, bringing his message of peace and hope with the most physical of the arts: sculpture."
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tvSobreAutor.setContentOffset(CGPoint.zero, animated: false)
    }
}
