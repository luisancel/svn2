//
//  TerminosViewController.swift
//  Deredia
//
//  Created by ice on 14/2/19.
//  Copyright © 2019 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit
import WebKit

class TerminosViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {

    @IBOutlet weak var webview: WKWebView!
    @IBOutlet weak var bCerrar: UIButton!
    @IBOutlet weak var mainView: UIView!
    
    var activityIndicator: UIActivityIndicatorView!
    
    @IBAction func btnCerrar(_ sender: Any) {
        
        self.dismiss(animated: true, completion: {
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.layer.cornerRadius = 10.0
        bCerrar.layer.cornerRadius = 10.0
        
        webview.navigationDelegate = self
        webview.uiDelegate = self

        activityIndicator = UIActivityIndicatorView()
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        
        view.addSubview(activityIndicator)
        
        let url = URL(string : "http://servrecursosapl.ice.go.cr/pubres/deredia/policy/Reglamento-App-Deredia-SJO.pdf")
        let request = URLRequest(url : url!)
        webview.load(request)
    }
    
    func showActivityIndicator(show: Bool) {
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        showActivityIndicator(show: false)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showActivityIndicator(show: true)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        showActivityIndicator(show: false)
    }
}
