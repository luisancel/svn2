//
//  EsculturaListaTableCell.swift
//  Deredia
//
//  Created by ice on 18/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class EsculturaListaTableCell: UITableViewCell {

    @IBOutlet weak var lblNombreObra: UILabel!
    
    @IBOutlet weak var lblNumeroObra: UILabel!
    
    @IBOutlet weak var imgObra: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
