//
//  ViewController.swift
//  Deredia
//
//  Created by Luis Eduardo Sanchez Celedon on 12/14/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit
import AVFoundation



class ViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool){
        //Tiempo para el splash
        Thread.sleep(forTimeInterval: 2.0)
        
        //Revision del token
        let token = UserDefaults.standard.object(forKey: "token")
        
        if token != nil{
            performSegue(withIdentifier: "id_video", sender: self)
        }else{
            performSegue(withIdentifier: "id_login", sender: self)
        }
        
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }

}

