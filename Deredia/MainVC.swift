//
//  MainVC.swift
//  Deredia
//
//  Created by Luis Eduardo Sanchez Celedon on 12/19/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var idsEsculturas: [String] = [String]()
    var idEscultura = ""
    
    @IBOutlet weak var idEsculturaPicker: UIPickerView!
    
    @IBOutlet weak var vwBuscarNumero: UIView!
    
    @IBOutlet weak var vwSanJose: UIView!
    
    @IBOutlet weak var vwMuseos: UIView!
    
    @IBOutlet weak var vwJade: UIView!
    
    @IBOutlet weak var vwPatrocinadores: UIView!
    
    @IBOutlet weak var vwSobreArtista: UIView!
    
    @IBOutlet weak var textFieldObra: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Museo de San Jose
        NotificationCenter.default.addObserver(self, selector: #selector(showSJMaps), name: NSNotification.Name("ShowSJMaps"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showSJObra), name: NSNotification.Name("ShowSJObra"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showSJExpo), name: NSNotification.Name("ShowSJExpo"), object: nil)
        // Museo de ORO
        NotificationCenter.default.addObserver(self, selector: #selector(showOroObra), name: NSNotification.Name("ShowOroObra"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showOroExpo), name: NSNotification.Name("ShowOroExpo"), object: nil)
        // Museo de Jade
        NotificationCenter.default.addObserver(self, selector: #selector(showJadeObra), name: NSNotification.Name("ShowJadeObra"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showJadeExpo), name: NSNotification.Name("ShowJadeExpo"), object: nil)

        // El artista
        NotificationCenter.default.addObserver(self, selector: #selector(showArtista), name: NSNotification.Name("ShowArtista"), object: nil)
        
        // Patrocinadores
        NotificationCenter.default.addObserver(self, selector: #selector(showPatro), name: NSNotification.Name("ShowPatro"), object: nil)
        
        // Términos y condiciones
        NotificationCenter.default.addObserver(self, selector: #selector(showTerm), name: NSNotification.Name("ShowTerminos"), object: nil)
        
        // Connect data:
        //self.idEsculturaPicker.delegate = self
        //self.idEsculturaPicker.dataSource = self
        
        idsEsculturas = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27"]
        createPickerEsculturas()
        dissmissPickerView()
        
        vwBuscarNumero.clipsToBounds = false
        vwBuscarNumero.layer.cornerRadius = 20
        vwBuscarNumero.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
    
        vwSanJose.clipsToBounds = false
        vwSanJose.layer.cornerRadius = 20
        vwSanJose.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        
        vwMuseos.clipsToBounds = false
        vwMuseos.layer.cornerRadius = 20
        vwMuseos.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        
        vwJade.clipsToBounds = false
        vwJade.layer.cornerRadius = 20
        vwJade.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        
        vwPatrocinadores.layer.cornerRadius = 10;
        vwPatrocinadores.layer.masksToBounds=true;

        vwSobreArtista.layer.cornerRadius = 10;
        vwSobreArtista.layer.masksToBounds=true;

        //self.idEsculturaPicker.layer.cornerRadius = 10;
        //self.idEsculturaPicker.layer.masksToBounds=true;
    }
    
    @objc func showSJMaps(){
        performSegue(withIdentifier: "ShowSJMaps", sender: nil)
    }
    
    @objc func showSJObra(){
        performSegue(withIdentifier: "ShowSJObra", sender: nil)
    }
    
    @objc func showSJExpo(){
        performSegue(withIdentifier: "ShowSJExpo", sender: nil)
    }
    
    @objc func showOroObra(){
        performSegue(withIdentifier: "ShowOroObra", sender: nil)
    }
    
    @objc func showOroExpo(){
        performSegue(withIdentifier: "ShowOroExpo", sender: nil)
    }
    
    @objc func showJadeObra(){
        performSegue(withIdentifier: "ShowJadeObra", sender: nil)
    }
    
    @objc func showJadeExpo(){
        performSegue(withIdentifier: "ShowJadeExpo", sender: nil)
    }
    
    @objc func showArtista(){
        performSegue(withIdentifier: "ShowArtista", sender: nil)
    }
    
    @objc func showPatro(){
        performSegue(withIdentifier: "ShowPatro", sender: nil)
    }
    @objc func showTerm(){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc : TerminosViewController = storyboard.instantiateViewController(withIdentifier: "TerminosViewController") as! TerminosViewController
        
        self.present(vc, animated: true, completion: nil)
    }

    @IBAction func onMoreTapped(){
        self.view.endEditing(true)
        
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }

    //Elementos visibles en el picker
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Numero de elementos en el picker
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return idsEsculturas.count
    }
    
    // Dato retornado para el componente (columna) que se esta pasando
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return idsEsculturas[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        idEscultura = idsEsculturas[row]
        
        textFieldObra.text = idEscultura
        
        //self.view.endEditing(true)
        
//        if (row > 0){
//            performSegue(withIdentifier: "id_audio_sj", sender: self)
//        }
    }

    //Se llama antes de performSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "id_audio_sj"){
            let infoObraVC = segue.destination as! SanJoseInfoEsculturaViewController //Casteo
            
            //Seteo del parametro a enviar
            infoObraVC.idEscultura = self.idEscultura
        }
    }
    
    // Para esconder el statusbar
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    func createPickerEsculturas(){
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textFieldObra.inputView = pickerView
    }
    
    
    func dissmissPickerView(){
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        
        let doneButton = UIBarButtonItem(title: " OK ", style: .plain, target: self, action: #selector(self.dissmissKeyboard))
        
        var titCanc = "CANCELAR"
        
        if (Locale.current.languageCode == "en"){
            titCanc = "CANCEL"
        }
        let cancelButton = UIBarButtonItem(title: titCanc, style: .plain, target: self, action: #selector(self.cancelKeyboard))
        toolBar.setItems([cancelButton, flexible, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.barTintColor = UIColor.black
        doneButton.tintColor = UIColor(red:0.73, green:0.62, blue:0.37, alpha:1.0)
        cancelButton.tintColor = UIColor(red:0.73, green:0.62, blue:0.37, alpha:1.0)
        textFieldObra.inputAccessoryView = toolBar
    }
    
    
    @objc func dissmissKeyboard(){
        if idEscultura == ""{
            idEscultura = "1"
        }
        performSegue(withIdentifier: "id_audio_sj", sender: self)
        view.endEditing(true)
    }
    
    @objc func cancelKeyboard(){
        view.endEditing(true)
    }
    
}
