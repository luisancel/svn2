//
//  SanJoseListaController.swift
//  Deredia
//
//  Created by ice on 23/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class SanJoseListaController: UIViewController {

    @IBOutlet weak var tblListaEsculturas: UITableView!
    
    var idEsculturaSeleccionada : String = ""
    var isErrorConsulta = false
    var isErrorComunicacion = false
    
    var esculturas: [Escultura] = []
    let baseURL = "http://servrecursosapl.ice.go.cr/wsderedia/ws/"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblListaEsculturas.delegate = self
        tblListaEsculturas.dataSource = self
        
        getEsculturasSJ()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (isErrorConsulta){
            var mensaje = "Revisa tu conexión a Internet"
            
            if (Locale.current.languageCode == "en"){
                mensaje = "Check your Internet Connection"
            }
                
            let alerta =  UIAlertController(title: "Error", message: mensaje, preferredStyle: UIAlertController.Style.alert)
            
            alerta.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                //No hay que hacer nada
            }))
            
            self.present(alerta, animated: true, completion: nil)
        }
        if (isErrorComunicacion){
            var mensaje = "Error al obtener respuesta del servidor"
            
            if (Locale.current.languageCode == "en"){
                mensaje = "Error retrieving information from server"
            }
            
            let alerta =  UIAlertController(title: "Error", message: mensaje, preferredStyle: UIAlertController.Style.alert)
            
            alerta.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                //No hay que hacer nada
            }))
            
            self.present(alerta, animated: true, completion: nil)
        }
    }

    func getEsculturasSJ(){
        let operacion = "media/listarsj"
        
        guard let url = URL(string: (baseURL + operacion)) else { return }
        
        URLSession.shared.dataTask(with: url){
            (data, response, error) in
            
            //Chequeo del error y que el status sea el 200
            if error != nil {
                let str = error!.localizedDescription
                if (str.contains("connect to the server")){
                    self.isErrorComunicacion = true
                }else{
                    self.isErrorConsulta = true
                }
                return
            }
            
            guard let data = data else { return }
            
            //Decodificacion y parseo del JSON
            do {
                //Decodificacion JSON a arreglo de esculturas
                self.esculturas = try JSONDecoder().decode([Escultura].self, from: data)
                
                //Get back to the main queue
                DispatchQueue.main.async {
                    if (self.esculturas.count == 0){
                        print("bla")
                        self.isErrorComunicacion = true
                    }else{
                        print("bli")
                        self.tblListaEsculturas.reloadData()
                    }
                }
                
            } catch let jsonError {
                print(jsonError)
            }
            }.resume()
    }

}

extension SanJoseListaController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return esculturas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let escultura = esculturas[indexPath.row]
        
        let celda = tableView.dequeueReusableCell(withIdentifier: "SanJoseTableCell") as! SanJoseTableCell
        
        //Deshabilitacion de touch
        //celda.selectionStyle = .none;
        //celda.isUserInteractionEnabled = false
        
        celda.lblNombreObraSJ.text = "- " + escultura.nombre
        celda.lblNumeroObraSJ.text = String(escultura.id)
        
        let url = URL(string:
            escultura.urlimg1.trimmingCharacters(in: .whitespacesAndNewlines))
        
        celda.imgObraSJ.kf.indicatorType = .activity
        celda.imgObraSJ.kf.setImage(with: url)
        
        return celda
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100.0;//Ancho de la fila
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.idEsculturaSeleccionada = String(esculturas[indexPath.row].id)
        
        performSegue(withIdentifier: "id_lista_sj_audio", sender: self)
    }
    
    //Se llama antes de performSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let infoObraVC = segue.destination as! SanJoseInfoEsculturaViewController //Casteo
        
        //Seteo del parametro a enviar
        infoObraVC.idEscultura = self.idEsculturaSeleccionada
    }

}
