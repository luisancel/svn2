//
//  SanJoseTableCell.swift
//  Deredia
//
//  Created by ice on 23/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class SanJoseTableCell: UITableViewCell {

    @IBOutlet weak var imgObraSJ: UIImageView!
    
    @IBOutlet weak var lblNombreObraSJ: UILabel!
    
    @IBOutlet weak var lblNumeroObraSJ: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        //print("Se selecciono la obra "+lblNumeroObraSJ.text!)
    }

}
