//
//  SanJoseInfoViewController.swift
//  Deredia
//
//  Created by Luis Eduardo Sanchez Celedon on 12/28/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class SanJoseInfoViewController: UIViewController {

    @IBOutlet weak var tvSanJoseActividades: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (Locale.current.languageCode == "en"){
            tvSanJoseActividades.text = "As part of the capital city’s cultural activities, the Municipality of San José promotes and organizes the exhibition entitled \"Jiménez Deredia. The Strength and Universality of the Sphere.\"  This event is an important return of the Costa Rican master to his native country, and a significant opportunity to present his work and artistic philosophy.  In recent years, master Jorge Jiménez Deredia has tried to decode Costa Rica’s cultural identity based on pre-Columbian spheres as a fundamental element.  An exhibition in the city of San José is an opportunity to use his works and thinking to convey a world-view on the spherical nature of life. More than an exhibition, it is a moment of reflection. Spectators can delve deeply into a vision of Costa Rican identity by coming into contact, at no cost, with large-scale sculptures and a structured thought process.\n\nIn this exhibition, Jiménez Deredia presents:\n\n- A series of genesis that narrate time and space through the transformation of matter.\n\n- An idea of environmental sculpture to describe cosmic participation \n\n - A spherical-circular vision of human beings\n\n- An organic, symbolic-transmutational sculpture that describes cosmic participation.\n\n- A sculpture that makes us aware that we are stardust in transmutation."
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tvSanJoseActividades.setContentOffset(CGPoint.zero, animated: false)
    }
}
