//
//  SanJoseInfoEsculturaViewController.swift
//  Deredia
//
//  Created by ice on 26/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit
import Kingfisher
import AVFoundation

//let baseURL = "http://201.203.21.234/wsderedia/ws/"
let baseURL = "http://servrecursosapl.ice.go.cr/wsderedia/ws/"

class SanJoseInfoEsculturaViewController: UIViewController {

    @IBOutlet weak var btnImg2: UIButton!
    
    @IBOutlet weak var btnImg1: UIButton!
    
    @IBOutlet weak var vwAudioControles: UIView!
    
    @IBOutlet weak var imgEscultura: UIImageView!
    
    @IBOutlet weak var lblEntreBotones: UILabel!
    
    @IBOutlet weak var txtNombreObraPrincipal: UILabel!
    
    @IBOutlet weak var txtDetalle: UITextView!
    
    @IBOutlet weak var txtNombreObraPlay: UILabel!
    
    @IBOutlet weak var txtDescripcion: UITextView!
    
    @IBOutlet weak var txtPeso: UITextView!
    
    @IBOutlet weak var txtDimensiones: UITextView!
    
    @IBOutlet weak var btnPlayPause: UIButton!
    
    var isErrorConsulta = false
    var isErrorComunicacion = false
    var idEscultura = ""
    var urlAudioString = ""
    var urlImg1 = ""
    var urlImg2 = ""
    var player : AVPlayer = AVPlayer()
    let obrasSolo1Img = ["11", "12", "13", "14", "16", "18", "21", "22", "24", "27"]
    
    @IBAction func btnPlayPauseAction(_ sender: Any) {
        
        if player.timeControlStatus == .playing {
            player.pause()
            
            btnPlayPause.setImage(UIImage(named: "play"), for: .normal)
        }else{
            player.play()
            
            btnPlayPause.setImage(UIImage(named: "pausa"), for: .normal)
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        vwAudioControles.clipsToBounds = false
        vwAudioControles.layer.cornerRadius = 20
        vwAudioControles.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        
        btnImg1.backgroundColor = UIColor.white
        btnImg1.layer.cornerRadius = 10
        btnImg1.clipsToBounds = true
        btnImg1.layer.borderWidth = 1
        btnImg1.layer.borderColor = UIColor.white.cgColor
        
        btnImg2.layer.cornerRadius = 10
        btnImg2.clipsToBounds = true
        btnImg2.layer.borderWidth = 1
        btnImg2.layer.borderColor = UIColor.white.cgColor
        
        if (!obrasSolo1Img.contains(idEscultura)){
            let lineView = UIView(
                frame: CGRect(x: 0,
                          y: lblEntreBotones.bounds.size.height / 2,
                          width: lblEntreBotones.bounds.size.width,
                          height: 1
                )
            )
            lineView.backgroundColor = UIColor.white
            lblEntreBotones.addSubview(lineView)
        }else{
            self.btnImg2.isHidden = true
        }
        
        let idioma : String = Locale.current.languageCode!
        
        let req : InfoObraRequest = InfoObraRequest(id: idEscultura, locale: idioma)
       
        getEsculturaInfo(requestData: req){ (error) in
            if let error = error {
                let str = error.localizedDescription
                if (str.contains("connect to the server")){
                    self.isErrorComunicacion = true
                }else{
                    self.isErrorConsulta = true
                }
                
                print(error.localizedDescription)
                //fatalError(error.localizedDescription)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (isErrorConsulta){
            var mensaje = "Revisa tu conexión a Internet"
            
            if (Locale.current.languageCode == "en"){
                mensaje = "Check your Internet Connection"
            }
            
            let alerta =  UIAlertController(title: "Error", message: mensaje, preferredStyle: UIAlertController.Style.alert)
        
            alerta.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                //Retorna a la vista anterior pues no se va a poder mostrar nada
                self.navigationController?.popViewController(animated: true)
            }))
        
            self.present(alerta, animated: true, completion: nil)
        }else{
            if (isErrorComunicacion){
                var mensaje = "Error al obtener respuesta del servidor"
                
                if (Locale.current.languageCode == "en"){
                    mensaje = "Error retrieving information from server"
                }
                
                let alerta =  UIAlertController(title: "Error", message: mensaje, preferredStyle: UIAlertController.Style.alert)
                
                alerta.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                    //Retorna a la vista anterior pues no se va a poder mostrar nada
                    self.navigationController?.popViewController(animated: true)
                }))
                
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    
    func getEsculturaInfo(requestData : InfoObraRequest, completion:((Error?) -> Void)?){
        
        let url = URL(string: baseURL + "media/consultarsj")
        
        //Especifica el request
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        // Indica codificacion JSON en el envio
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        request.allHTTPHeaderFields = headers
        
        // Codifica el objeto requestData a JSON
        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(requestData)
            // Coloca el json en el body
            request.httpBody = jsonData
        } catch {
            completion?(error)
        }
        
        // Crea una sesion HTTP para enviar la solicitud POST con el JSON interno
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            guard responseError == nil else {
                completion?(responseError!)
                return
            }
            
            // Valida su existe informacion legible en los datos de respuesta
            if let data = responseData, let _ = String(data: data, encoding: .utf8) {
                
                do{
                    let escult = try JSONDecoder().decode(Escultura.self, from: data)
                    
                    //Get back to the main queue
                    DispatchQueue.main.async {
                        let url = URL(string:
                            escult.urlimg1.trimmingCharacters(in: .whitespacesAndNewlines))
                        
                        self.imgEscultura.kf.indicatorType = .activity
                        self.imgEscultura.kf.setImage(with: url)
                        
                        self.txtNombreObraPrincipal.text = escult.nombre
                        self.txtNombreObraPlay.text = escult.nombre
                        self.txtDetalle.text = escult.desc
                        self.txtPeso.text = self.txtPeso.text + ": " + escult.peso!
                        self.txtDimensiones.text = self.txtDimensiones.text + ": " + escult.dimension!
                        self.txtDescripcion.text = escult.texto
                        
                        self.urlAudioString = escult.urlaudio!
                        self.urlImg1 = escult.urlimg1
                        self.urlImg2 = escult.urlimg2!
                        
                        self.confPlayer()
                    }
                }catch{
                    print("Error de parseo")
                }
            } else {
                print("Informacion no legible en respuesta")
            }
        }
        task.resume()
    }
    
    /*
     * Se aplica (para reproduccion Streaming http) esta llave en plist
     *
     <key>NSAppTransportSecurity</key>
     <dict>
     <key>NSAllowsArbitraryLoads</key>
     <true/>
     </dict>
     
     */
    func confPlayer(){
        let url  = URL.init(string: self.urlAudioString)
        
        let playerItem: AVPlayerItem = AVPlayerItem(url: url!)
        player = AVPlayer(playerItem: playerItem)
        
        //Notificador para cuando el audio ha terminado
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: nil) // Agrega el observer
        
        let playerLayer = AVPlayerLayer(player: player)
        
        playerLayer.frame = CGRect(x: 0, y: 0, width: 10, height: 50)
        self.view.layer.addSublayer(playerLayer)
    }
    
    @IBAction func btnImg2Accion(_ sender: Any) {

        btnImg2.backgroundColor = UIColor.white
        //btnImg2.titleLabel?.textColor = UIColor.init(displayP3Red: 186, green: 159, blue: 95, alpha: 1)
        
        btnImg1.backgroundColor = UIColor.clear
        //btnImg1.titleLabel?.textColor = UIColor.white

        let url = URL(string:
            self.urlImg2.trimmingCharacters(in: .whitespacesAndNewlines))
        
        self.imgEscultura.kf.indicatorType = .activity
        self.imgEscultura.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                //print(image)
                //print(error?.code)
                //print(imageURL )
                
            })
    }
    
    @IBAction func btnImg1Accion(_ sender: Any) {
        btnImg1.backgroundColor = UIColor.white
        //btnImg1.titleLabel?.textColor = UIColor.init(displayP3Red: 186, green: 159, blue: 95, alpha: 1)
        
        btnImg2.backgroundColor = UIColor.clear
        //btnImg2.titleLabel?.textColor = UIColor.white
        
        let url = URL(string:
            self.urlImg1.trimmingCharacters(in: .whitespacesAndNewlines))
        
        self.imgEscultura.kf.indicatorType = .activity
        self.imgEscultura.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
            //print(image)
            //print(error?.code)
            //print(imageURL )
            
        })
    }
    
    // Manejo de notificacion
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        btnPlayPause.setImage(UIImage(named: "play"), for: .normal) //Pone el boton de play al fin del audio
    }
    // Remueve el observer
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
