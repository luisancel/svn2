//
//  SideMenuVC.swift
//  Deredia
//
//  Created by Luis Eduardo Sanchez Celedon on 12/19/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class SideMenuVC: UITableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        switch indexPath.row {
        case 0:
    NotificationCenter.default.post(name: NSNotification.Name("ShowSJMaps"), object: nil)
        case 1:
            NotificationCenter.default.post(name: NSNotification.Name("ShowSJObra"), object: nil)
        case 2:
            NotificationCenter.default.post(name: NSNotification.Name("ShowSJExpo"), object: nil)
        case 4:
            NotificationCenter.default.post(name: NSNotification.Name("ShowOroObra"), object: nil)
        case 5:
            NotificationCenter.default.post(name: NSNotification.Name("ShowOroExpo"), object: nil)
        case 7:
            NotificationCenter.default.post(name: NSNotification.Name("ShowJadeObra"), object: nil)
        case 8:
            NotificationCenter.default.post(name: NSNotification.Name("ShowJadeExpo"), object: nil)
        case 9:
            NotificationCenter.default.post(name: NSNotification.Name("ShowArtista"), object: nil)
        case 10:
            NotificationCenter.default.post(name: NSNotification.Name("ShowPatro"), object: nil)
        case 11:
            NotificationCenter.default.post(name: NSNotification.Name("ShowTerminos"), object: nil)
        default: break
        }

    }

    // Para esconder el statusbar
    override var prefersStatusBarHidden: Bool{
        return true
    }
    @IBAction func btnCerrarMenuAccion(_ sender: Any) {
        //El indice 3 al no tener accion cierra automaticamente el menu
        let indexPath = IndexPath(row: 3, section: 0)
         self.tableView.delegate?.tableView!(self.tableView, didSelectRowAt: indexPath)
        
    }
}
