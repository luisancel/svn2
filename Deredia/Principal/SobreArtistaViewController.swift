//
//  SobreArtistaViewController.swift
//  Deredia
//
//  Created by ice on 27/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit
import Kingfisher

class SobreArtistaViewController: ViewController {

    @IBOutlet weak var imgArtista: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: "http://201.203.21.234/pubres/deredia/otro/artista.png")
        
        imgArtista.kf.indicatorType = .activity
        imgArtista.kf.setImage(with: url)
    }
}
