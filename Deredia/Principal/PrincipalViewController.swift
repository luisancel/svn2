//
//  PrincipalViewController.swift
//  Deredia
//
//  Created by ice on 27/12/18.
//  Copyright © 2018 Instituto Costarricense de Electricidad. All rights reserved.
//

import UIKit

class PrincipalViewController: UIViewController {

    var idEscultura = "3"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    func seleccionNumEscultura(){
        //Aqui falta la asignacion de self.idEscultura por el valor que tiene el componente UI
        performSegue(withIdentifier: "idSegue", sender: self)
    }

    //Se llama antes de performSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var infoObraVC = segue.destination as! SanJoseInfoEsculturaViewController //Casteo
        
        //Seteo del parametro a enviar
        infoObraVC.idEscultura = self.idEscultura
    }
}
